import inputs from './libs/inputs'

import VumikForm from './VumikForm'
import VumikInput from './VumikInput'
import VumikInputBox from './inputs/VumikInputBox'
import VumikInputButton from './inputs/VumikInputButton'
import VumikInputFile from './inputs/VumikInputFile'
import VumikInputSelect from './inputs/VumikInputSelect'
import VumikInputText from './inputs/VumikInputText'
import VumikInputTextArea from './inputs/VumikInputTextArea'

class Vumik {
  constructor () {
    this.options = {
      components: {
        VumikForm,
        VumikInput,
        VumikInputBox,
        VumikInputButton,
        VumikInputFile,
        VumikInputSelect,
        VumikInputText,
        VumikInputTextArea
      },
      inputs
    }
  }

  install (Vue) {
    Vue.prototype.$vumik = this

    for (const component in this.options.components) {
      Vue.component(component, this.options.components[component])
    }
  }

  categorize (type) {
    return this.options.library?.[type]?.categorization || 'uncategorized'
  }

  component (type) {
    return this.options.inputs?.[type]?.component || false
  }
}

export default new Vumik()
