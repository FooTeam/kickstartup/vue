const vi = 'VumikInput'
const join = c => ({
  categorization: c.toLowerCase(),
  component: vi + c
})
export default {
  // SINGLE LINE TEXT INPUTS
  ...[
    'color',
    'date',
    'datetime-local',
    'email',
    'hidden',
    'month',
    'number',
    'password',
    'search',
    'tel',
    'text',
    'time',
    'url',
    'week'
  ].reduce((inputs, type) => ({ ...inputs, [type]: join('Text') }), {}),
  // MULTI LINE TEXT INPUTS
  textarea: join('TextArea'),
  // BOX STYLE INPUTS
  checkbox: join('Box'),
  radio: join('Box'),
  // BUTTON STYLE INPUTS
  submit: join('Button'),
  button: join('Button'),
  // SELECT STYLE INPUTS
  select: join('Select'),
  // FILE INPUTS
  file: join('File'),
  image: join('File')
}
