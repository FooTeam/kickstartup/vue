export function objEquals (a, b) {
  return JSON.stringify(a) === JSON.stringify(b)
}

export function has (ctx, prop) {
  return Object.prototype.hasOwnProperty.call(ctx, prop)
}

export function empty (value) {
  if (typeof value === 'number') {
    return false
  }
  return (
    value === undefined || value === null || value === '' || value === false
  )
}
