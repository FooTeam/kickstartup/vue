import { empty, has, objEquals } from './utils'

export default class Form {
  constructor (ctx) {
    this.form = new Map()
    this.ctx = ctx
  }

  add (field, component) {
    this.form.set(field, component)
    return this
  }

  has (field) {
    return this.form.has(field)
  }

  get (field) {
    return this.form.get(field)
  }

  register (field, component) {
    this.add(field, component)
    const hasValue = has(component.$options.propsData, 'value')
    if (this.ctx.hasInitialValue && !empty(this.ctx.initialValues[field])) {
      component.ctx.model = this.ctx.initialValues[field]
    }
    if (
      hasValue &&
            !objEquals(component.proxyValue, this.ctx.initialValues[field])
    ) {
      this.ctx.setFieldValue(field, component.proxyValue)
    }
  }

  data () {
    return {
      proxyValue: {},
      form: this,
      register: this.register.bind(this)
    }
  }
}

export function formProviders (ctx) {
  return {
    fieldRegister: ctx.register,
    fieldSetter: ctx.setFieldValue
  }
}

export function formComputed () {
  return {
    hasInitialValue () {
      return this.vumikValue && typeof this.vumikValue === 'object'
    },
    initialValues () {
      if (
        has(this.$options.propsData, 'vumikValue') &&
                typeof this.vumikValue === 'object'
      ) {
        return { ...this.vumikValue }
      }
      return {}
    }
  }
}

export function formMethods () {
  return {
    applyInitialValues () {
      if (this.hasInitialValue) {
        this.proxyValue = this.initialValues
      }
    },
    setFieldValue (field, value) {
      if (value === undefined) {
        const { [field]: value, ...proxy } = this.proxyValue
        this.proxyValue = proxy
      } else {
        this.proxyValue = { ...this.proxyValue, [field]: value }
      }
      this.$emit('input', { ...this.proxyValue })
    },
    setValues (values) {
      const keys = [
        ...Object.keys(values),
        ...Object.keys(this.proxyValue)
      ]
      keys.forEach(field => {
        if (!objEquals(values[field], this.proxyValue[field])) {
          this.setFieldValue(field, values[field])
          if (
            this.form.has(field) &&
                        !objEquals(
                          values[field],
                          this.form.get(field).proxyValue
                        )
          ) {
            this.form.get(field).ctx.model = values[field]
          }
        }
      })
    }
  }
}
