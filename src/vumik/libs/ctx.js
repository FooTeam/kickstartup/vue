import { objEquals } from './utils'

export default {
  ctx () {
    return defineModelProperty.call(this, {
      attributes: this.attributes,
      categorization: this.categorization,
      component: this.component,
      label: this.label,
      listeners: this.listeners,
      name: this.nameOrDefault,
      type: this.type,
      value: this.value
    })
  },
  attributes,
  listeners,
  nameOrDefault,
  hasName
}

function attributes () {
  const attrs = this.$attrs
  if (this.hasName) {
    attrs.name = this.name
  }
  return attrs
}

function listeners () {
  const { input, ...listeners } = this.$listeners
  return listeners
}

function nameOrDefault () {
  if (this.name === true && this.categorization !== 'button') {
    return this.type
  }
  if (
    this.name === false ||
        (this.categorization === 'button' && this.name === true)
  ) {
    return false
  }
  return this.name
}

function hasName () {
  return typeof this.name !== 'boolean'
}

function defineModelProperty (ctx) {
  return Object.defineProperty(ctx, 'model', {
    get: modelGetter.bind(this),
    set: modelSetter.bind(this),
    enumerable: true
  })
}

function modelGetter () {
  if (!this.proxyValue && this.proxyValue !== 0) {
    return ''
  }
  return this.proxyValue
}

function modelSetter (value) {
  let didUpdate = false
  if (!objEquals(this.proxyValue, value)) {
    this.proxyValue = value
    didUpdate = true
  }
  if (this.ctx.name && typeof this.fieldSetter === 'function') {
    this.fieldSetter(this.ctx.name, value)
  }
  if (didUpdate) {
    this.$emit('input', value)
  }
}
