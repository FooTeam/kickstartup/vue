export default {
  props: {
    ctx: {
      type: Object,
      required: true
    }
  },
  computed: {
    type () {
      return this.ctx.type
    },
    value () {
      return this.ctx.value
    },
    attributes () {
      return this.ctx.attributes || {}
    }
  }
}
