import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import authRoutes from './auth/index'
import projectRoutes from './projects/index'
import dashboardRoutes from './dashboard/index'
import store from '../store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Root',
    component: Home,
    meta: {
      public: true
    }
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
    meta: {
      public: true
    }
  },
  {
    path: '/about',
    name: 'About',
    component: About,
    meta: {
      public: true
    }
  },
  ...authRoutes,
  ...projectRoutes,
  ...dashboardRoutes
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

const isAuthorize = (route, next) => {
  if (route.meta.roles) {
    if (
      route.meta.roles.some(
        role => store.getters['user/roles'].indexOf(role) !== -1
      )
    ) {
      if (
        store.getters['user/status'] > 2 ||
        store.getters['user/roles'].includes('ROLE_ADMIN')
      ) {
        next()
      } else {
        next({ name: 'Register' })
      }
    } else {
      next({ name: 'Home' })
    }
  }
}

router.beforeEach(async (to, from, next) => {
  if (to.path === '/' && store.getters['auth/isLoggedIn']) {
    return next({ name: 'Dashboard' })
  }

  if (!to.meta.public) {
    if (store.getters['auth/isLoggedIn']) {
      !store.getters['user/roles']
        ? store
          .dispatch('user/me')
          .then(() => {
            isAuthorize(to, next)
          })
          .catch(e => next({ name: 'Login' }))
        : isAuthorize(to, next)
    } else {
      next({ name: 'Login' })
    }
  } else {
    next()
  }
})

export default router
