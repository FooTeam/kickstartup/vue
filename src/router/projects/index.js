import Projects from '../../views/projects/Projects.vue'
import CreateProject from '../../views/projects/CreateProject'
import ProjectsDetail from '../../views/projects/ProjectsDetail'
import ProjectsListUser from '../../views/projects/ProjectsListUser'
import UpdateProject from '../../views/projects/UpdateProject'

export default [
  {
    path: '/projects',
    name: 'ProjectsListing',
    component: Projects,
    meta: {
      public: true
    }
  },
  {
    path: '/projects/:id',
    name: 'ProjectsDetail',
    component: ProjectsDetail,
    meta: {
      public: true
    }
  },
  {
    path: '/dashboard/projects/:id',
    name: 'ProjectsDetailUser',
    component: ProjectsDetail,
    meta: {
      roles: ['ROLE_USER']
    }
  },
  {
    path: '/dashboard/projects',
    name: 'ProjectsListUser',
    component: ProjectsListUser,
    meta: {
      roles: ['ROLE_USER']
    }
  },
  {
    path: '/dashboard/projects/create',
    name: 'CreateProject',
    component: CreateProject,
    meta: {
      roles: ['ROLE_USER']
    }
  },
  {
    path: '/dashboard/projects/update/:id',
    name: 'UpdateProject',
    component: UpdateProject,
    meta: {
      roles: ['ROLE_USER']
    }
  }
]
