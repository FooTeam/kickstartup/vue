import Dashboard from '../../views/dashboard/Dashboard.vue'
import Users from '../../views/dashboard/User/Users.vue'
import UsersDetail from '../../views/dashboard/User/Profil/UsersDetail.vue'
import Discussions from '../../views/dashboard/Discussion/Discussions.vue'

export default [
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard,
    meta: {
      roles: ['ROLE_USER']
    }
  },
  {
    path: '/dashboard/users',
    name: 'Users',
    component: Users,
    meta: {
      roles: ['ROLE_ADMIN']
    }
  },
  {
    path: '/dashboard/users/:id',
    name: 'UsersProfil',
    component: UsersDetail,
    meta: {
      roles: ['ROLE_ADMIN'],
      page: 'profile'
    }
  },
  {
    path: '/dashboard/users/:id/application',
    name: 'UsersApplication',
    component: UsersDetail,
    meta: {
      roles: ['ROLE_ADMIN'],
      page: 'application'
    }
  },
  {
    path: '/dashboard/discussions',
    name: 'UserDiscussions',
    component: Discussions,
    meta: {
      roles: ['ROLE_USER'],
      page: 'discussions'
    }
  }
]
