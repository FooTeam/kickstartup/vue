import graphql from './graphql/auth'
import api from './apiPlatform/auth'

export default {
  login (data) {
    switch (localStorage.getItem('api')) {
      case 'graphql':
        return graphql.login(data)
      case 'apiPlatform':
        return api.login(data)
      default:
        return {
          token: 'token',
          user: { username: 'test42' }
        }
    }
  },
  register (data) {
    switch (localStorage.getItem('api')) {
      case 'graphql':
        return graphql.register(data)
      case 'apiPlatform':
        return api.register(data)
      default:
        return {
          token: 'token',
          user: { username: 'test42' }
        }
    }
  },
  createApplication () {
    switch (localStorage.getItem('api')) {
      case 'graphql':
        return graphql.createApplication()
      case 'apiPlatform':
        return api.createApplication()
    }
  },
  updateApplication (data) {
    switch (localStorage.getItem('api')) {
      case 'graphql':
        return graphql.updateApplication(data)
      case 'apiPlatform':
        return api.updateApplication(data)
    }
  }
}
