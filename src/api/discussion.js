import graphql from './graphql/discussion'
import api from './apiPlatform/discussion'

export default {
  create (data) {
    switch (localStorage.getItem('api')) {
      case 'graphql':
        return graphql.create(data)
      case 'apiPlatform':
        return api.create(data)
    }
  },
  getUserDiscussions (data) {
    switch (localStorage.getItem('api')) {
      case 'graphql':
        return graphql.getUserDiscussions(data)
    }
  }
}
