import graphql from './graphql/user'
import api from './apiPlatform/user'

export default {
  me (username) {
    switch (localStorage.getItem('api')) {
      case 'graphql':
        return graphql.me()
      case 'apiPlatform':
        return api.me(username)
    }
  },
  update (data) {
    switch (localStorage.getItem('api')) {
      case 'graphql':
        return graphql.update(data)
      case 'apiPlatform':
        return api.update(data)
    }
  },
  all () {
    switch (localStorage.getItem('api')) {
      case 'graphql':
        return graphql.all()
      case 'apiPlatform':
        return api.all()
    }
  },
  get (id) {
    switch (localStorage.getItem('api')) {
      case 'graphql':
        return graphql.get(id)
      case 'apiPlatform':
        return api.get(id)
    }
  }
}
