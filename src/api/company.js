import graphql from './graphql/company'
import api from './apiPlatform/company'

export default {
  create (data) {
    switch (localStorage.getItem('api')) {
      case 'graphql':
        return graphql.create(data)
      case 'apiPlatform':
        return api.create(data)
    }
  }
}
