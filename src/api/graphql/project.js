import graphqlClient from './../../utils/graphql'
import { gql } from 'apollo-boost'

export default {
  async create (data) {
    return graphqlClient
      .mutate({
        mutation: gql`
          mutation CreateProject($project: CreateProject!) {
            createProject(project: $project) {
              title
              description
              email
              phoneNumber
              company {
                id
                name
                localisation
              }
            }
          }
        `,
        variables: { project: data }
      })
      .then(res => res.data.createProject)
  },
  async update (data) {
    return graphqlClient
      .mutate({
        mutation: gql`
          mutation UpdateProject($project: UpdateProject!) {
            updateProject(project: $project) {
              id
              description
              email
              phoneNumber
              title
              status
            }
          }
      `,
        variables: { project: data }
      })
      .then(res => res.data.updateProject)
  },
  async fetchAll () {
    return graphqlClient
      .mutate({
        mutation: gql`
        query {
          getAllProjects {
            id
            title
            status
            description
            email
            phoneNumber
            company {
              id
              name
              localisation
            }
          }
        }
      `
      })
      .then(res => res.data.getAllProjects)
  },
  async get (id) {
    return graphqlClient
      .mutate({
        mutation: gql`
          query Project($id: ID) {
            project(id: $id) {
              id
              title
              status
              description
              phoneNumber
              email
              company {
                id
                name
                localisation
              }
            }
          }
        `,
        variables: { id: id }
      })
      .then(res => res.data.user)
  }
}
