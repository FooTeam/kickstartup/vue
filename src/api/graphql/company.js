import graphqlClient from './../../utils/graphql'
import { gql } from 'apollo-boost'

export default {
  async create (data) {
    return graphqlClient
      .mutate({
        mutation: gql`
          mutation CreateCompany($company: CreateCompany!) {
            createCompany(company: $company) {
              id
              name
              siren
              nic
              creationDate
              localisation
            }
          }
        `,
        variables: { company: data }
      })
      .then(res => res.data.createCompany)
  }
}
