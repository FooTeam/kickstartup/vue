import graphqlClient from '../../utils/graphql'
import { gql } from 'apollo-boost'

export default {
  create (data) {
    return graphqlClient
      .mutate({
        mutation: gql`
        mutation CreateDiscussion($discussion: CreateDiscussionInput!) {
          createDiscussion(discussion: $discussion) {
            id
          }
        }
      `,
        variables: { discussion: data }
      }).then(res => res.data.createDiscussion)
  },
  getUserDiscussions (userId) {
    return graphqlClient
      .query({
        query: gql`
        query getUserDiscussions($userId: ID) {
          getUserDiscussions(userId: $userId) {
            id,
            project {
              id,
              title,
              company {
                id,
                name,
              }
            },
            createdAt,
          }
        }
      `,
        variables: { userId }
      }).then(res => res.data.getUserDiscussions)
  }
}
