import graphqlClient from './../../utils/graphql'
import { gql } from 'apollo-boost'

export default {
  async login (data) {
    return graphqlClient
      .mutate({
        mutation: gql`
          mutation Login($email: String!, $password: String!) {
            login(email: $email, password: $password) {
              token
            }
          }
        `,
        variables: data
      })
      .then(res => res.data.login)
  },
  async register (data) {
    return graphqlClient
      .mutate({
        mutation: gql`
          mutation SignUp($email: String!, $password: String!, $name: String!, $role: String!) {
            signup(email: $email, password: $password, name: $name, role: $role) {
              token
            }
          }
        `,
        variables: data
      })
      .then(res => res.data.signup)
  },
  async updateApplication (data) {
    const application = { application: data }
    return graphqlClient
      .mutate({
        mutation: gql`
          mutation UpdateApplication($application: ApplicationInput!) {
            updateApplication(application: $application) {
              fullName
              companyName
              companySiret
              companyLocalisation
              comment
              applicant {
                id
              }
            }
          }
        `,
        variables: application
      })
      .then(res => res.data.updateApplication)
  },
  async createApplication () {
    return graphqlClient
      .mutate({
        mutation: gql`
          mutation {
            createApplication {
              id
              fullName
              companyName
              companySiret
              companyLocalisation
              comment
            }
          }
        `
      })
      .then(res => res.data.createApplication)
  }
}
