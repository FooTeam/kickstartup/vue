import graphqlClient from './../../utils/graphql'
import { gql } from 'apollo-boost'

export default {
  async me () {
    return graphqlClient
      .mutate({
        mutation: gql`
          query {
            me {
              id
              email
              roles
              name
              status
              createdAt
              firstname
              lastname
              company {
                id
                name
                siren
                nic
                creationDate
                localisation
              }
              application {
                fullName
                companyName
                companySiret
                companyLocalisation
                comment
              }
            }
          }
        `
      })
      .then(res => res.data.me)
  },
  async update (data) {
    return graphqlClient
      .mutate({
        mutation: gql`
          mutation UpdateUser($user: UpdateUser!) {
            updateUser(user: $user) {
              id
              email
              roles
              name
              status
              createdAt
              firstname
              lastname
              company {
                id
                name
                siren
                nic
                creationDate
                localisation
              }
              application {
                fullName
                companyName
                companySiret
                companyLocalisation
                comment
              }
            }
          }
        `,
        variables: { user: data }
      })
      .then(res => res.data.updateUser)
  },
  async all () {
    return graphqlClient
      .mutate({
        mutation: gql`
          query {
            getAllUsers {
              id
              name
              firstname
              lastname
              status
              roles
              createdAt
              company {
                id
                name
              }
            }
          }
        `
      })
      .then(res => res.data.getAllUsers)
  },
  async get (id) {
    return graphqlClient
      .mutate({
        mutation: gql`
          query User($id: ID) {
            user(id: $id) {
              id
              email
              roles
              name
              status
              firstname
              lastname
              createdAt
              company {
                id
                name
                siren
                nic
                creationDate
                localisation
              }
              application {
                fullName
                companyName
                companySiret
                companyLocalisation
                comment
              }
            }
          }
        `,
        variables: { id: id }
      })
      .then(res => res.data.user)
  }
}
