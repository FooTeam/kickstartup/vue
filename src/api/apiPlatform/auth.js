export default {
  async login (data) {
    const init = {
      method: 'POST',
      headers: { 'Content-type': 'application/json' },
      body: JSON.stringify(data),
      mode: 'cors'
    }

    return fetch(`${process.env.VUE_APP_API_PLATFORM_URL}/login`, init).then((res) =>
      res.json()
    ).catch(e => console.log(e.message))
  },
  async register (data) {
    const roles = ['ROLE_USER']
    switch (data.role) {
      case 'investor':
        roles.push('ROLE_INVESTOR')
        break
      case 'entrepreneur':
        roles.push('ROLE_ENTREPRENEUR')
        break
      case 'admin':
        roles.push('ROLE_ADMIN')
        break
    }

    const init = {
      method: 'POST',
      headers: { 'Content-type': 'application/json' },
      body: JSON.stringify({ ...data, status: 0, roles }),
      mode: 'cors'
    }

    return fetch(`${process.env.VUE_APP_API_PLATFORM_URL}/users`, init).then((res) =>
      res.json()
    ).catch(e => console.log(e.message))
  }
}
