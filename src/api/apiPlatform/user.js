export default {
  async me (username) {
    const init = {
      method: 'GET',
      headers: { 'Content-type': 'application/json', Authorization: 'Bearer ' + localStorage.getItem('token') },
      mode: 'cors'
    }

    return fetch(`https://localhost:8443/users/me/${username}`, init).then((res) =>
      res.json()
    ).catch(e => console.log(e.message))
  },
  async update (data) {
    //
  },
  async all () {
    //
  },
  async get (id) {
    //
  }
}
