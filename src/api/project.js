import graphql from './graphql/project'
import api from './apiPlatform/project'

export default {
  create (data) {
    switch (localStorage.getItem('api')) {
      case 'graphql':
        return graphql.create(data)
      case 'apiPlatform':
        return api.create(data)
    }
  },
  update (data) {
    switch (localStorage.getItem('api')) {
      case 'graphql':
        return graphql.update(data)
      case 'apiPlatform':
        return api.update(data)
    }
  },
  fetchAll () {
    switch (localStorage.getItem('api')) {
      case 'graphql':
        return graphql.fetchAll()
      case 'apiPlatform':
        return api.fetchAll()
    }
  },
  get (id) {
    switch (localStorage.getItem('api')) {
      case 'graphql':
        return graphql.get(id)
      case 'apiPlatform':
        return api.get(id)
    }
  }
}
