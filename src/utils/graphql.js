import ApolloClient from 'apollo-boost'

export default new ApolloClient({
  uri: process.env.VUE_APP_GRAPHQL_URL,
  request: async operation => {
    operation.setContext({
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token')
      }
    })
  }
})
