import projectAPI from '../../api/project'
import {
  ADD_PROJECT,
  SET_PROJECTS,
  UPDATE_PROJECT
} from '../mutation-types'

const state = () => ({
  allProjects: []
})

const getters = {
  getOne: state => id => state?.allProjects.find(project => project.id === id),
  allProjects: state => state?.allProjects
}

const actions = {
  createProject ({ commit, rootState }, data) {
    delete data.submit
    if (rootState.auth.token !== undefined) {
      return projectAPI.create(data).then(data => {
        commit(ADD_PROJECT, data)
      })
    }
  },
  fetchAll ({ commit }) {
    return projectAPI.fetchAll().then(data => {
      commit(SET_PROJECTS, data)
    })
  },
  updateProject ({ commit, rootState }, data) {
    delete data.submit
    if (rootState.auth.token !== undefined) {
      return projectAPI.update(data).then(data => {
        commit(UPDATE_PROJECT, data)
      })
    }
  }
}

const mutations = {
  [ADD_PROJECT] (state, project) {
    state.allProjects.push(project)
  },
  [SET_PROJECTS] (state, projects) {
    state.allProjects = projects
  },
  [UPDATE_PROJECT] (state, project) {
    const index = state.allProjects.findIndex(entry => entry.id === project.id)
    state.allProjects[index] = project
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
