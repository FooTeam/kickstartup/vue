import companyAPI from '../../api/company'
import userAPI from '../../api/user'
import {
  ADD_OWN_COMPANY,
  SET_TOKEN,
  SET_USER,
  SET_USERS,
  ADD_USERS,
  SET_APPLICATION
} from '../mutation-types'

const state = () => ({
  me: undefined,
  all: []
})

const getters = {
  roles: state => state?.me?.roles,
  status: state => state?.me?.status,
  all: state => state?.all,
  getOne: state => id => state?.all.find(user => user.id === id),
  validation: state => state?.me?.application,
  me: state => state?.me
}

const actions = {
  me ({ state, commit, rootState }) {
    if (rootState.auth.token !== undefined) {
      return userAPI
        .me(rootState.auth.user.username)
        .then(data => {
          commit(SET_USER, { me: data })
        })
        .catch(e => {
          commit(`auth/${SET_TOKEN}`, { token: undefined }, { root: true })
          commit(`auth/${SET_USER}`, { user: undefined }, { root: true })
          throw new Error(e.message)
        })
    }
  },
  update ({ state, commit }, data) {
    delete data.submit
    return userAPI.update(data).then(data => {
      if (data.id === state.me.id) {
        commit(SET_USER, { me: data })
      }
    })
  },
  createCompany ({ commit }, data) {
    delete data.submit
    return companyAPI.create(data).then(data => {
      commit(ADD_OWN_COMPANY, data)
    })
  },
  fetchAll ({ commit, rootState }) {
    if (rootState.auth.token !== undefined) {
      return userAPI.all().then(data => {
        commit(SET_USERS, data)
      })
    }
  },
  fetchOne ({ commit, rootState }, id) {
    if (rootState.auth.token !== undefined) {
      return userAPI.get(id).then(data => {
        commit(ADD_USERS, data)
      })
    }
  }
}

const mutations = {
  [SET_USER] (state, { me }) {
    state.me = me
  },
  [ADD_OWN_COMPANY] (state, company) {
    if (state.me.companies) {
      state.me.companies.push(company)
    } else {
      state.me.companies = [company]
    }
  },
  [SET_USERS] (state, users) {
    state.all = users
  },
  [ADD_USERS] (state, user) {
    const index = state.all.findIndex(entry => entry.id === user.id)
    if (index !== -1) {
      state.all[index] = user
    } else {
      state.all.push(user)
    }
  },
  [SET_APPLICATION] (state, application) {
    console.log(application)
    const index = state.all.findIndex(
      entry => entry.id === application.applicant.id
    )

    state.all[index].application = application
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
