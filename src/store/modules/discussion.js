import discussionAPI from '../../api/discussion'
import { ADD_DISCUSSION, SET_DISCUSSIONS } from '../mutation-types'

const state = {
  discussions: []
}

const getters = {
  allDiscussions: state => state.discussions
}

const actions = {
  createDiscussion ({ commit }, data) {
    return discussionAPI.create(data)
      .then(res => {
        commit(ADD_DISCUSSION, res.id)
        return res.id
      })
  },
  getUserDiscussions ({ commit, rootState }) {
    return discussionAPI.getUserDiscussions(rootState.user.me.id)
      .then(res => {
        commit(SET_DISCUSSIONS, res)
        return res
      })
  }
}

const mutations = {
  [ADD_DISCUSSION] (state, discussion) {
    state.discussions.push(discussion)
  },
  [SET_DISCUSSIONS] (state, discussions) {
    state.discussions = discussions
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
