import auth from '../../api/auth'
import {
  SET_TOKEN,
  SET_USER,
  SET_APPLICATION
} from '../mutation-types'

const state = () => ({
  token:
    localStorage.getItem('token') !== null && localStorage.getItem('token') !== 'undefined'
      ? localStorage.getItem('token')
      : undefined,
  user: localStorage.getItem('user') !== null && localStorage.getItem('token') !== 'undefined'
    ? JSON.parse(localStorage.getItem('user'))
    : undefined
})

const getters = {
  isLoggedIn: state => state.token !== undefined,
  username: state => state?.user?.username,
  roles: state => state?.user?.role,
  token: state => state?.token
}

const actions = {
  login ({ commit }, { email, password }) {
    return auth.login({ email, password }).then(data => {
      commit(SET_TOKEN, { token: data.token })
      commit(SET_USER, { user: { username: email } })
    })
  },
  register ({ commit }, { email, password, repeatPassword, name, role }) {
    if (password === repeatPassword) {
      auth.register({ email, password, name, role }).then(data => {
        console.log(data)
        commit(SET_TOKEN, { token: data.token })
        commit(SET_USER, { user: { username: email } })
      })
    } else {
      console.log('Bad repeat password')
    }
  },
  logout ({ commit }) {
    commit(SET_TOKEN, { token: undefined })
    commit(SET_USER, { user: undefined })
  },
  createApplication ({ commit }) {
    return auth.createApplication().then(data => {
      commit(SET_APPLICATION, data)
    })
  },
  updateApplication ({ commit }, data) {
    return auth.updateApplication(data).then(data => {
      console.log(data)
      commit(SET_APPLICATION, data)
      console.log(data)
      commit(`user/${SET_APPLICATION}`, data, { root: true })
    })
  }
}

const mutations = {
  [SET_TOKEN] (state, { token }) {
    state.token = token
    localStorage.setItem('token', token)
  },
  [SET_USER] (state, { user }) {
    state.user = user
    localStorage.setItem('user', JSON.stringify(user))
  },
  [SET_APPLICATION] (state, application) {
    state.application = application
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
