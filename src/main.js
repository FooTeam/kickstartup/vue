import Vue from 'vue'
import Vumik from './vumik/Vumik'
import App from './App.vue'
import store from './store'
import router from './router'
import '@/assets/styles/index.css'

window.API = 'graphql'

Vue.config.productionTip = false

Vue.use(Vumik)

new Vue({
  render: (h) => h(App),
  router,
  store
}).$mount('#app')
